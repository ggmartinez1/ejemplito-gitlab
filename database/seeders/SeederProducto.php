<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class SeederProducto extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for($a = 0; $a < 600; $a++){
            DB::table('productos')->insert([
                'nombre' => Str::random(10),
                'descripcion' => Str::random(100),
                'stock' => 10,
            ]);
        }


        DB::table('productos')->insert([
            'nombre' => "Asado de tira",
            'descripcion' => "Del bueno",
            'stock' => 100,
        ]);
    }
}
