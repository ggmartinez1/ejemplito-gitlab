<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Producto;

class ProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return $user = Producto::factory()->make([
            'nombre' => Str::random(10),
            'descripcion' => Str::random(100),
            'stock' => 10
        ]);
    }
}
